
#VARIABLES RSYNC

SSH_PORT=22
SOURCE_DIR_FILE=""
USER_DEST_FINAL="traitement"
DEST_FINAL_DIR=""
IP_DEST=""

#VARIABLES LOGS
DATE_NOW=$(date)
LOGS="$0.log"

#COMMAND
echo "INITIALISATION DE L'ENVOIE DES FICHIERS..."
echo "COPIER LA SOURCE ${SOURCE_DIR_FILE} ==> ${USER_DEST_FINAL}@${IP_DEST}:${DEST_FINAL_DIR}/ ?"
read -p "Continuer ? y/n " REP

if [ ${REP} == y ]
then
     rsync -a -v -z -P -e "ssh -p ${SSH_PORT}" ${SOURCE_DIR_FILE}/ ${USER_DEST_FINAL}@${IP_DEST}:${DEST_FINAL_DIR}/

#START
     if [ $? -ne 0 ]
     then
          echo "Yo Bro ! Je pense que un petit soucis est arrivé lors du transfert !"
          echo "Retente en vérifiant les paramètres du script ! ;)"
          echo "${DATE_NOW} : Error ! Transfert échoué" >> ${LOGS}

     else
          echo "${DATE_NOW} : Good !! Tous les fichiers ont été transférés en toute sécurité par ssh ! Bye Bye !" | tee -a ${LOGS}
     fi
else
     echo "Bye ! Bye !"
fi


